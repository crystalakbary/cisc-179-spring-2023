import spacy
import numpy as np
import pandas as pd

# Load the language model
nlp = spacy.load('en_core_web_sm')

# Load the CSV file containing the questions and answers
qa_df = pd.read_csv("questions_and_answers.csv")

# Extract the questions and answers from the DataFrame
questions = qa_df["question"].tolist()
answers = qa_df["answer"].tolist()

# Define a function to ask a question and compare the user's answer to the correct answer
def ask_question(question, correct_answer, user_answer):
    # Convert the correct_answer and user_answer to vector representations
    correct_answer_vector = nlp(correct_answer).vector
    user_answer_vector = nlp(user_answer).vector

    # Calculate the cosine similarity between the correct_answer and user answer vectors using numpy
    similarity_score = np.dot(correct_answer_vector, user_answer_vector) / (np.linalg.norm(correct_answer_vector) * np.linalg.norm(user_answer_vector))
    return similarity_score

# Ask each question and keep track of the user's total score
results = []
for i, question in enumerate(questions):
    correct_answer = answers[i]
    user_answer = input(question + " ")
    score = ask_question(question, correct_answer, user_answer)
    result = {
        "question": question,
        "user_answer": user_answer,
        "correct_answer": correct_answer,
        "score": score
    }
    results.append(result)

# Create a DataFrame from the lod and save to disk as CSV
df = pd.DataFrame(results)
df.to_csv("results.csv", index=False)

# Load the CSV files back into DataFrames
results_df = pd.read_csv("results.csv")

# Print the user's final score
total_score = results_df["score"].sum()
print("Your score is:", total_score * 100 / len(questions))
