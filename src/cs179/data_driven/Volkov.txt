entryway:You are standing at the entrance to a dark and creepy castle. The door behind you has closed and locked, leaving you no choice but to venture inside. You can go north to the hallway or east to the dining room.:hallway,dining room
hallway:You find yourself in a long and narrow hallway. The walls are lined with torches, casting flickering shadows on the stone floor. You can go north to the library, south to the entryway, or east to the kitchen.:library,entryway,kitchen
dining_room:You enter a grand dining room with a long wooden table in the center. The table is set for a feast, but the food looks stale and rotten. You can go west to the entryway.:entryway
kitchen:You step into a large kitchen with pots and pans hanging from the ceiling. The smell of burnt food fills the air. You can go west to the hallway.:hallway
library:You find yourself in a library filled with dusty books and cobwebs. A few rays of light filter through the dusty windows. You can go south to the hallway or east to the laboratory.:hallway,laboratory
treasure:You have found the treasure! A chest overflowing with gold and jewels is in front of you. You can go back west to the hallway.:hallway
bedroom:You enter a cozy bedroom with a large four-poster bed. The room is dimly lit by a flickering candle on the nightstand. You can go down to the cellar.:cellar
dungeon:You find yourself in a dark and damp dungeon. The air is thick with the smell of rotting wood and rusted metal. You can go up to the entryway.:entryway
tower:You climb a narrow spiral staircase to the top of a tall tower. The view from the window is breathtaking, but you feel a chill in the air. You can go down to the garden.:garden
garden:You step into a lush garden filled with colorful flowers and a babbling brook. The tranquility of the setting belies the danger lurking nearby. You can go up to the tower or south to the laboratory.:tower,laboratory
cellar:You descend a creaky staircase into a dimly lit cellar. The walls are lined with dusty bottles and cobweb-covered barrels. You can go up to the bedroom or east to the laboratory.:bedroom,laboratory
laboratory:You enter a strange laboratory filled with bubbling beakers and strange contraptions. A mad scientist cackles maniacally in the corner. You can go west to the library, north to the garden, or west to the cellar.:library,garden,cellar
forest: You find yourself in a dense forest with towering trees and a thick canopy overhead. You can hear the sound of running water in the distance. You can go north to the river or south to the meadow.:river,meadow
river: You stand at the edge of a wide river with fast-moving water. You can see a boat on the other side. You can go south to the forest or try to swim across the river to the boat.:forest,boat
boat: You are on a small boat in the middle of the river. The water is choppy, and you can hear the sound of rushing rapids up ahead. You can go back north to the river or continue downstream to the rapids.:river,rapids
rapids: You find yourself in the middle of a set of dangerous rapids. The boat is bouncing wildly, and you can hear the sound of crashing water ahead. You can try to steer the boat to safety or abandon it and swim to shore.:shore
shore: You wash up on a rocky shore, battered and bruised but alive. You can see a path leading up to a nearby hill, or you can explore the beach.:hill,beach
meadow: You enter a vast meadow filled with wildflowers and tall grass. The sun is shining overhead, and you can hear birds singing in the distance. You can go north to the forest or south to the cave.:forest,cave
cave: You approach a dark cave, its entrance shrouded in shadow. You can feel a chill running down your spine. You can go north to the meadow or venture inside the cave.:meadow,underground_lake
underground_lake: You find yourself in an underground lake surrounded by towering rock walls. The water is clear, but you can't see the bottom. You can swim across the lake or try to climb the walls to reach the surface.:cliff
cliff: You reach the top of the cliff and emerge into a new area.:cliff
