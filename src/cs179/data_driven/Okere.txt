"You arrive at the location and stand in front of this enormous gate, it is about 30 ft high and is connected to walls equally as hight that surrounds the house like a midevil wall did back in the day."

"You start to hear noises as the gate begins to creek and open, however to your surprise the gate opens upwards, the already tall gate proceeds to get tall an taller till it stops at bout double it's original height. You're amazed at how the gate is seemingly floating in the air. This is truly and engineering marvel you think to yourself."

"You start towards the seemingly empty entrance only to be met by some sort of obstacle. You start pushing against it and realize that there is in fact a door there and that is is invisble, you feel around the for a door knob to no avail. You start looking around for a clue of some sort and find the literal door password right above the door. it reads Dǎkāi, the door prompts you for the password as if it knows that you've seen it. You're a bit startled but state the password anyways and proceed."

"You make your way into the hallway past the door, you are met with two dirrections. To the right the floor transitions from marble to carpet, and two the left he floor continues on as marble for as far as the eye can see."

"You walk toward the carpeted area that lead you to another hallway with 2 doors that you can see from where your standing however you know there's probably more than meets the eye."

"The first room seems to be the master suite, it has a colloasal king sized bed with an ivory bed headboard. There is a beautiful wood dresser with an enormous mirror lined with carpet on the exterior. There's another door, from what you can see it is the bathroom. The walls look yellow. Kind of like a shade of gold."

"The second room looks like a home workout area. You see gold dumbbells, treddmills, and benches. This goes together with the black colored ground, it seems to be made of a spongey texture. Propably to help minimize injuries in case of accidents and damages to the equipment."

"You make your way further along the marbled floor and see the living room. the Tv is enourmous and there is a sectional with about a bajillion pillows on it. The center coffee table looks like it was made out of diamond the way it shined and sparkled. You scan the living room some more and are greeted by many paintings, one looks like a centuar, but instead of the head of a man and a body of a horse it was a head of a shark and a body of a human. It really makes you wonder what wealthy people think about sometimes."
