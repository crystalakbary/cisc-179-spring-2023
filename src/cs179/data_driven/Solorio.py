def start_game():
    print("Welcome to the text adventure game!")
    print("You wake up in a dark room. You can't see anything.")
    print("What do you want to do?")
    print("1. Look around.")
    print("2. Try to find a light switch.")
    print("3. Scream for help.")

    choice = input("Enter your choice (1, 2, or 3): ")

    if choice == "1":
        print("You reach around in the darkness and eventually find a door.")
        room_2()
    elif choice == "2":
        print("You find a light switch and turn on the lights.")
        room_1()
    elif choice == "3":
        print("Nobody hears you. You are stuck here forever.")
        end_game()

def room_1():
    print("You are in a room with a table and a chair.")
    print("There is a piece of paper on the table.")
    has_read_paper = False
    while True:
        print("What do you want to do?")
        print("1. Read the paper.")
        print("2. Look under the table.")
        print("3. Leave the room.")
        choice = input("Enter your choice (1, 2, or 3): ")
        if choice == "1":
            if not has_read_paper:
                print("The paper reads: 'The code is 2012'.")
                has_read_paper = True
            else:
                print("You have already read the paper.")
        elif choice == "2":
            print("You find a key under the table.")
            room_2()
        elif choice == "3":
            start_game()


def room_2():
    print("You are in a hallway.")
    print("There are two doors, one on the left and one on the right.")
    print("What do you want to do?")
    print("1. Try the door on the left.")
    print("2. Try the door on the right.")
    print("3. Go back to the previous room.")

    choice = input("Enter your choice (1, 2, or 3): ")

    if choice == "1":
        print("You try the door on the left, but it's locked.")
        room_2()
    elif choice == "2":
        print("You try the door on the right, and it opens into a treasure room!")
        end_game()
    elif choice == "3":
        room_1()

def room_3():
    print("You are in a small library.")
    print("There are bookshelves lining the walls.")
    print("What do you want to do?")
    print("1. Search the bookshelves.")
    print("2. Sit down and read a book.")
    print("3. Go back to the previous room.")

    choice = input("Enter your choice (1, 2, or 3): ")

    if choice == "1":
        print("You search the bookshelves and find a hidden passage behind them.")
        room_4()
    elif choice == "2":
        print("You pick up a book and start reading, losing track of time.")
        print("When you look up, the room has transformed into a different place.")
        end_game()
    elif choice == "3":
        room_2()

def room_4():
    print("You crawl through the hidden passage and find yourself in a secret chamber.")
    print("There is a mysterious artifact on a pedestal in the center of the room.")
    print("What do you want to do?")
    print("1. Examine the artifact.")
    print("2. Take the artifact.")
    print("3. Leave the room.")

    choice = input("Enter your choice (1, 2, or 3): ")

    if choice == "1":
        print("As you examine the artifact, you feel a strange energy emanating from it.")
        print("Suddenly, the room begins to shake, and the artifact crumbles into dust.")
        print("You barely make it out of the room before it collapses completely.")
        end_game()
    elif choice == "2":
        print("You take the artifact, but as soon as you do, the room starts to crumble.")
        print("You make a mad dash for the exit, narrowly escaping the collapsing chamber.")
        end_game()
    elif choice == "3":
        room_2()

def end_game():
    print("Congratulations, you have won the game!")

# Start the game
start_game()

