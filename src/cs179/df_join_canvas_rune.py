# from colorama import init
# import canvas_grab
# from pathlib import Path
# from termcolor import colored
# from canvasapi.exceptions import ResourceDoesNotExist

from canvasapi import Canvas
from dotenv import load_dotenv, dotenv_values
import pandas as pd

from cs179.config import PRIVATE_DIR, CANVAS_API_URL

# Initialize a new Canvas object
load_dotenv()
ENV = dotenv_values()

canvas = Canvas(CANVAS_API_URL, ENV['CANVAS_API_KEY'])
print(canvas)


# interactive, noupdate, config = .get_options()
# canvas = config.endpoint.login()
courses = list(canvas.get_courses())
print(courses)

course_id = next(iter([c.id for c in courses if 'CISC 179' in c.name]))
print(course_id)

course = canvas.get_course(course_id)
grade_changes = course.get_grade_change_events()
print(grade_changes)

sections = list(course.get_sections())
assert len(sections) == 1
section = sections[0]
print(section)

student_summaries = list(course.get_course_level_student_summary_data())
print(student_summaries[0])

enrollments = list(section.get_enrollments())


# df_users
df_can = pd.DataFrame([enr.user for enr in enrollments])
df_can

df_rune = pd.read_csv(
    PRIVATE_DIR / '2023-02-24-wk3-number-of-interactions-data_for_sdccd_mesa_college_cs179_spring23.csv',
    index_col=0, header=0).T
df_rune = pd.DataFrame(
    df_rune.values[2:],
    index=df_rune.index.values[2:],
    columns=df_rune.values[0] + ': ' + df_rune.values[1]
)
df_rune

df_rune_act = pd.read_csv(
    PRIVATE_DIR / '2023-02-24-wk3-data_for_sdccd_mesa_college_cs179_spring23.csv',
    index_col=0, header=0).T
df_rune_act = pd.DataFrame(
    df_rune_act.values[2:],
    index=df_rune_act.index.values[2:],
    columns=df_rune_act.values[0] + ': ' + df_rune_act.values[1]
)

"""
df_nums = df_num_interacts.iloc[2:].astype(float)
dfnums = dfnums.fillna(0)
dfnums.index = [tuple(s.split('<br>')[0].lower().split(',') + s.split('<br>')[-1][1:-1]) for s in dfnums.index.values]
dfnums.index = [tuple(s.split('<br>')[0].lower().split(',') + [s.split('<br>')[-1][1:-1]]) for s in dfnums.index.values]
dfnums.index
dfnums.index.values

pd.DataFrame(dfnums.index.values)
pd.DataFrame(list(x) for x in dfnums.index.values])
pd.DataFrame([list(x) for x in dfnums.index.values])
df_runstone_roster= pd.DataFrame([list(x) for x in dfnums.index.values])
df_runestone_roster= pd.DataFrame([list(x) for x in dfnums.index.values])
df_runestone_roster.columns= ['last', 'first', 'username']
dfnums.index.values
df_num_interacts.iloc[2: ].astype(float)
df_users
df_users['last']= [df_users['name'].str.split()[-1]]
df_users[['first', 'last']]= df_users['name'].str.split()
df_users['name'].str.split()
df_users['last']= [n[-1] for n in df_users['name'].str.split()]
df_users[['name', 'last']]
[n for n in dfnums['last'] if n in df_users['last'].str.lower()]
dfnums.columns
dfnums
[n[1] for n in dfnums.index if n in df_users['last'].str.lower()]
[n[0] for n in dfnums.index if n in df_users['last'].str.lower()]
[n for n in df_users['last'].str.lower()]
[n[1] for n in dfnums.index]
[n[0] for n in dfnums.index]
[n[0] for n in dfnums.index if n[0] in df_users['last'].str.lower()]
[n[0] for n in dfnums.index if n[0] in df_users['last'].str.lower().str.strip()]
set(df_users['last'].str.lower().str.strip())
userlastset= set(df_users['last'].str.lower().str.strip())
runelastset= set(n[0] for n in dfnums.index)
runelastset.intersection(userlastset)
userlastlist= list(df_users['last'].str.lower().str.strip())
df_users['last']= userlastlist
df_run= dfnums
df_rune= dfnums
df_rune
df_rune['last']= [i[0] for i in df_rune.index]
df_rune['last']= [i[0] for i in list(df_rune.index.values)]
df_rune['last']
df_rune.join(df_users, on='last', how='outer')
df_rune.join(df_users, on='last', how='inner')
df_rune.index
df_rune['fullname']= df_rune.index.values
df_rune['first']= [x[1] for x in df_rune.index.values]
df_rune['runeuser']= [x[-1] for x in df_rune.index.values]
df_rune
df.reset_index()
df_rune.reset_index()
df_rune.reset_index(drop=True)
df_rune= df_rune.reset_index(drop=True)
df_users
df_users.join?
df_users.join(df_rune, on='last')
df_rune.columns= [str(x).strip() for x in df_rune.columns]
df_users.columns= [str(x).strip() for x in df_users.columns]
df_users.join(df_rune, on='last')
df_rune.set_index('last')
df_rune= df_rune.set_index('last')
df_users= df_rune.set_index('last')
df_users
df_users= df_users.set_index('last')
pd.concat([df_users, df_rune], axis=1)
len(df_users.index)
len(set(df_users.index))
sorted(df_users.index)
df_users.loc['student']
idx= [s + f'{i}' if s == 'student' else s for i, s in enumerate(df_users.index)]
np.array(idx) == df_users.index.values
import numpy as np
np.array(idx) == df_users.index.values
idx
df_users.index= idx
df_users
pd.concat([df_users, df_rune], axis=1)
df_canvas_rune_join= pd.concat([df_users, df_rune], axis=1)
df_canvas_rune_join.to_csv('df_users_canvas_runestone_joined.csv')
ls
hist - o - p - f 'df_users_canvas_runestone_joined.csv.hist.ipy'
hist - f 'df_users_canvas_runestone_joined.csv.hist.py'
ls
hist - f df_users_canvas_runestone_joined.csv.hist.py
hist - o - p - f df_users_canvas_runestone_joined.csv.hist.ipy
ls
rm 'df_users_canvas_runestone_joined.csv'
ls
df_canvas_rune_join.to_csv('df_users_canvas_runestone_joined.csv')
rm "'df_users_canvas_runestone_joined.csv.hist.*'"
rm "'df_users_canvas_runestone_joined.csv.hist.ipy'"
rm "'df_users_canvas_runestone_joined.csv.hist.py'"
ls
mv df_users_ * .. / cs179 / data / private/
"""
